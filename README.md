![DSA Logo][DSA Logo]

# DSA Charakter Sammlung

## Struktur
Pro Spieler ein Unterordner (mit Namen des Spielers).

## Spieler
- [Alvina ???](./Alvina/Alvina_???.pdf)
- [Julbralos Sohn des Tesch](./Julbralos/Julbralos_Sohn_des_Tesch.pdf)
- [Praulodan Fuxfell](./Praulodan/Praulodan_Fuxfell.pdf)
- ...

## Techstack
Es sollte mindestens die [Optolith-Heldenverwaltung]
JSON-Datei sowie eine PDF-Version den Heldenbogens abgelegt werden.
Alles übrige ist dem Spieler überlassen.

## Nützliche Links
- [DSA Wiki] - Nützlich für die schnelle Suche von bspw. Sonderfertigkeiten,
Waffen, Regeln usw.
- [Optolith-Heldenverwaltung] - Zur Erstellung und Verwaltung von
Heldenbögen.
- [Fantasy Cities] - Karten-Generierung für Städte, Dorfer usw.
- [Aves Pfade] - Routernplanung in und Karte von Aventurien.


[DSA Logo]: https://upload.wikimedia.org/wikipedia/commons/0/09/Logo_das_Schwarze_Auge.svg "DSA Logo"
[DSA Wiki]: https://ulisses-regelwiki.de/index.php
[Aves Pfade]: http://www.avespfade.de/
[Fantasy Cities]: http://fantasycities.watabou.ru/
[Optolith-Heldenverwaltung]: https://www.ulisses-ebooks.de/product/209711/Optolith-Heldenverwaltung&language=de